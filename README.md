#Gerenciador de Funcionários

---
Autor: Arthur Busqueiro
---
## Descrição 

Criar uma API e Front para gerenciamento de Funcionários com os seguintes serviços;

a. Serviço que localiza/retorna funcionários por Nome;

b. Serviço que localiza/retorna funcionários por CPF;

c. Serviço que localiza/retorna funcionários por Cargo;

d. Serviço que localiza/retorna funcionários por Data de Cadastros;

e. Serviço que retorna funcionários agrupados por UF de Nascimento, de forma quantitativa;

f. Serviço que localiza/retorna funcionários por faixa salarial;

g. Serviço que localiza/retorna funcionários por status;

h. Serviço para incluir um novo funcionário (caso o funcionário já exista, apenas atualizar);

i. Serviço para excluir um funcionário pelo número do CPF;

Para solução, foi criada uma API RESTFul com formato JSON em NodeJS com banco de dados
MongoDB.

A API é divida em Controllers, Models, Repositories e Routes.

As Routes são as rotas registradas na API e indicam para qual Controller a API deve enviar os dados.

As Controllers manipulam os dados que vêm das requisições, realizando validações e enfim chamando um método do Repository 

Os Repositories são responsáveis por realizar as consultas e alterações no banco de dados e retornar

As Models são as entidades do banco de dados com suas propriedades


O MongoDB permite a manipulação de JSON com uma grande performance, e por isso a escolha da linguagem e banco.
