var should = require("should");
var request = require("request");
var chai = require("chai");
var expect = chai.expect;
var urlBase = "http://localhost:3000/";
const CPF = require("@fnando/cpf/dist/node");

describe("Teste API Funcionários", function () {
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var text = "";
    for (let index = 0; index < 10; index++) {
        text = "";
        for (var i = 0; i < 3; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        it("Serviço que localiza/retorna funcionários pelo Nome " + text, function (done) {
            request.get({
                    url: urlBase + "workers/name/" + text
                },
                function (error, response, body) {

                    var _body = {
                        workers: []
                    };
                    try {
                        _body = JSON.parse(body);
                    } catch (e) {
                        _body = {
                            workers: []
                        };
                    }
                    expect(response.statusCode).to.equal(200);

                    _body.workers.forEach(element => {
                        expect(element.Name).to.contain('Aaron');
                        expect(element.Name).to.be.a('string');
                        expect(element.CPF).to.be.a('string');
                        expect(new Date(element.RegistrationDate)).to.be.a('date');
                    });

                    done();
                }
            );
        });

    }


    it("Serviço que localiza/retorna funcionários pelo Nome Arthur", function (done) {
        request.get({
                url: urlBase + "workers/name/Arthur"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);

                _body.workers.forEach(element => {
                    expect(element.Name).to.contain('Arthur');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });

    var numbers = '1234567890';
    for (index = 0; index < 10; index++) {
        text = "";
        for (i = 0; i < 4; i++)
            text += numbers.charAt(Math.floor(Math.random() * numbers.length));
        it("Serviço que localiza/retorna funcionários pelo CPF " + text, function (done) {
            request.get({
                    url: urlBase + "workers/cpf/364"
                },
                function (error, response, body) {

                    var _body = {
                        workers: []
                    };
                    try {
                        _body = JSON.parse(body);
                    } catch (e) {
                        _body = {
                            workers: []
                        };
                    }
                    expect(response.statusCode).to.equal(200);
                    _body.workers.forEach(element => {
                        expect(element.CPF).to.contain('364');
                        expect(element.Name).to.be.a('string');
                        expect(element.CPF).to.be.a('string');
                        expect(new Date(element.RegistrationDate)).to.be.a('date');
                    });

                    done();
                }
            );
        });
    }
    it("Serviço que localiza/retorna funcionários pelo Cargo", function (done) {
        request.get({
                url: urlBase + "workers/job/Dev Jr"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);

                _body.workers.forEach(element => {
                    expect(element.Job).to.contain('Dev Jr');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários pela data de cadastro", function (done) {
        request.get({
                url: urlBase + "workers/registration/2017-04-08"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(new Date(element.RegistrationDate).getTime()).to.be.equal((new Date(2017, 3, 8).getTime()));
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });

    var workers = 0;
    it("Serviço que retorna funcionários agrupados por UF de Nascimento, de forma quantitativa", function (done) {
        request.get({
                url: urlBase + "workers/uf"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(worker => {
                    expect(worker.flatsCount).to.be.a('number');
                    workers += worker.flatsCount;
                });
                done();
            }
        );
    });


    it("Serviço que localiza/retorna funcionários por faixa salarial 0 a 1000", function (done) {
        request.get({
                url: urlBase + "workers/salary/0/1000"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Salary).to.be.most(1000);
                    expect(element.Salary).to.be.least(0);
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por faixa salarial 1000 a 5000", function (done) {
        request.get({
                url: urlBase + "workers/salary/1000/5000"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Salary).to.be.most(5000);
                    expect(element.Salary).to.be.least(1000);
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por faixa salarial 5000 a 20000", function (done) {
        request.get({
                url: urlBase + "workers/salary/5000/20000"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Salary).to.be.most(20000);
                    expect(element.Salary).to.be.least(5000);
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por faixa salarial maior que 20000", function (done) {
        request.get({
                url: urlBase + "workers/salary/20000"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Salary).to.be.most(20000);
                    expect(element.Salary).to.be.least(5000);
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });

    it("Serviço que localiza/retorna funcionários por Status Ativo", function (done) {
        request.get({
                url: urlBase + "workers/status/ativo"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Status).to.be.contain('ATIVO');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por Status Inativo", function (done) {
        request.get({
                url: urlBase + "workers/status/inativo"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Status).to.contain('INATIVO');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por Status Bloqueado", function (done) {
        request.get({
                url: urlBase + "workers/status/bloqueado"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }

                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Status).to.be.contain('BLOQUEADO');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    var _id;
    var cpf = CPF.generate();
    var newWorker = null;
    var date = new Date();
    it("Serviço para incluir um novo funcionário", function (done) {
        request.post({
                url: urlBase + "workers",
                json: {
                    "CPF": cpf,
                    "Job": "PO Sr " + cpf,
                    "Name": "Arthur Busqro " + cpf,
                    "RegistrationDate": date,
                    "Salary": 500,
                    "Status": "ATIVO",
                    "UF": "MG"
                }
            },
            function (error, response, body) {
                newWorker = body.worker;
                expect(response.statusCode).to.equal(200);
                expect(body.worker.Status).to.be.equal("ATIVO");
                expect(body.worker.Name).to.be.equal("Arthur Busqro " + cpf);
                expect(body.worker.CPF).to.be.equal(cpf);
                expect(body.worker.Job).to.be.equal("PO Sr " + cpf);
                expect(new Date(body.worker.RegistrationDate).getTime()).to.be.equal(date.getTime());
                expect(body.worker.Salary).to.be.equal(500);
                expect(body.worker.UF).to.be.equal("MG");
                _id = body.worker._id;
                done();
            });
    });

    it("Serviço para incluir um novo funcionário com dados incorretos", function (done) {
        request.post({
                url: urlBase + "workers",
                json: {
                    "CPF": "11655588866546",
                    "Job": "",
                    "Name": "Arthur",
                    "RegistrationDate": new Date(),
                    "Salary": 'SAS',
                    "Status": "III",
                    "UF": "ASD"
                }
            },
            function (error, response, body) {

                expect(response.statusCode).to.equal(400);
                expect(body.message.length).to.be.least(1);
                done();
            });
    });

    it("Serviço que localiza/retorna todos os funcionários", function (done) {
        request.get({
                url: urlBase + "workers/"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                if (newWorker != null){
                    expect(_body.workers.length).to.equal(workers + 1);
                }else{
                    expect(_body.workers.length).to.equal(workers);
                }
                _body.workers.forEach(element => {
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
});