﻿# API
---
Essa API foi criada para a prova de Admissão no Luizalabs
---
## Autor: Arthur Busqueiro

---
##Descrição

Criar uma API para gerenciamento de Funcionários com os seguintes serviços:

a. Serviço que localiza/retorna funcionários por Nome;

b. Serviço que localiza/retorna funcionários por CPF;

c. Serviço que localiza/retorna funcionários por Cargo;

d. Serviço que localiza/retorna funcionários por Data de Cadastros;

e. Serviço que retorna funcionários agrupados por UF de Nascimento, de forma quantitativa;

f. Serviço que localiza/retorna funcionários por faixa salarial;

g. Serviço que localiza/retorna funcionários por status;

h. Serviço para incluir um novo funcionário (caso o funcionário já exista, apenas atualizar);

i. Serviço para excluir um funcionário pelo número do CPF;

---

## INSTALAÇÃO

1. Instale o NodeJS caso ainda não possua. (https://nodejs.org/en/download/)

2. Com um terminal, navegue até a pasta onde se encontra o projeto extraído

3. Rode o comando 'npm install' para instalar as dependências do projeto

4. Rode a aplicação com o comando 'npm start'

5. Se tudo ocorrer bem, a aplicação estará rodando na porta 'http://localhost:3000/'

6. Para testar, acesse a url 'http://localhost:3000/' no navegador. 

7. O resultado deve ser '{"title":"Api running success!","version":"1.0.0"}'

----
## TESTES

1. A API possui testes unitários utilizando a biblioteca Mocha.

2. Verifique se a biblioteca e suas dependências estão corretamente instaladas.

3. npm install mocha -g --save-dev

4. npm install chai --save-dev

5. npm install should --save-dev

6. npm install request --save-dev

7. Utilize o comando 'npm test' para rodar os testes.