var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require('mongoose'),
    Task = require('../models/worker'),
    bodyParser = require('body-parser');
const config = require('../config');

var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('../swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Conexão com o Banco
mongoose.Promise = global.Promise;
mongoose.connect(config.connectionString, {
    useNewUrlParser: true
});

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.listen(port);


// Habilita o CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token, Authorization');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

//Importar rotas
const indexRoute = require('../routes/index');
const funcionariosRoute = require('../routes/workers');

app.use('/', indexRoute);
app.use('/workers', funcionariosRoute);