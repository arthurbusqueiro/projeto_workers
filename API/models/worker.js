'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const workerSchema = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        ref: 'Worker'
    },
    RegistrationDate: {
        type: Date,
        required: true,
        default: Date.now
    },
    Job: {
        type: String,
        required: true
    },
    CPF: {
        type: String,
        required: true,
        unique: true
    },
    Name: {
        type: String,
        required: true
    },
    UF: {
        type: String,
        required: true,
        enum: ['AC',
            'AL',
            'AP',
            'AM',
            'BA',
            'CE',
            'DF',
            'ES',
            'GO',
            'MA',
            'MT',
            'MS',
            'MG',
            'PA',
            'PB',
            'PR',
            'PE',
            'PI',
            'RJ',
            'RN',
            'RS',
            'RO',
            'RR',
            'SC',
            'SP',
            'SE',
            'TO'
        ],
        default: 'SP'
    },
    Salary: {
        type: Number,
        required: true,
        default: 0.0
    },
    Status: {
        type: String,
        default: 'ATIVO'
    }
});

module.exports = mongoose.model('Worker', workerSchema);