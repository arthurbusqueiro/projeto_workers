﻿# APP
---
Front criado para consumir a API de funcionários
---
Autor: Arthur Busqueiro

##Descrição

Criar uma FRONT para consumir os seguintes serviços da API:

a. Serviço que localiza/retorna funcionários por Nome;

b. Serviço que localiza/retorna funcionários por CPF;

c. Serviço que localiza/retorna funcionários por Cargo;

d. Serviço que localiza/retorna funcionários por Data de Cadastros;

e. Serviço que retorna funcionários agrupados por UF de Nascimento, de forma quantitativa;

f. Serviço que localiza/retorna funcionários por faixa salarial;

g. Serviço que localiza/retorna funcionários por status;

h. Serviço para incluir um novo funcionário (caso o funcionário já exista, apenas atualizar);

i. Serviço para excluir um funcionário pelo número do CPF;

## INSTALAÇÃO:

1. Instale o NodeJS caso ainda não possua. (https://nodejs.org/en/download/)

2. Com um terminal, navegue até a pasta onde se encontra o projeto extraído

3. Rode o comando 'npm install' para instalar as dependências do projeto

3. Rode o comando 'npm install -g @angular/cli' para instalar o Angular-CLI

4. Rode a aplicação com o comando 'ng serve'

5. Se tudo ocorrer bem, a aplicação estará rodando na porta 'http://localhost:4200/'

6. Garanta que a API está rodando na URL 'http://localhost:3000'

   Caso não esteja, a URL da API pode ser alterada no arquivo 'environments.ts'

7. Para testar, acesse a url 'http://localhost:4200/' no navegador. 