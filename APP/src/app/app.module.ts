import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

//Prime
import { CalendarModule } from 'primeng/calendar';
import { DialogModule } from 'primeng/dialog';
import { GrowlModule } from 'primeng/growl';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';
import { SliderModule } from 'primeng/slider';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';


import { environment } from '../environments/environment';
import { AppRouting } from './app.routing';
import { WorkersComponent } from 'src/app/workers/workers.component';
import { MessageService } from 'primeng/components/common/messageservice';

@NgModule({
  declarations: [
    AppComponent,
    WorkersComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CalendarModule,
    DialogModule,
    FormsModule,
    GrowlModule,
    HttpClientModule,
    InputTextModule,
    KeyFilterModule,
    ReactiveFormsModule,
    RouterModule,
    SliderModule,
    TableModule,
    ToastModule,
    AppRouting
  ],
  providers: [{ provide: 'environments', useValue: environment }, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
