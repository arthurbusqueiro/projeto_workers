import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkersService {

  constructor(private _httpClient: HttpClient) { }

  getAll() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(environment.apiUrl + 'workers', { headers: headers });
  }

  findByName(name) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(environment.apiUrl + 'workers/name/' + name, { headers: headers });
  }

  findByCPF(cpf) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(environment.apiUrl + 'workers/cpf/' + cpf, { headers: headers });
  }

  findByJob(job) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(environment.apiUrl + 'workers/job/' + job, { headers: headers });
  }

  groupByUF() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(environment.apiUrl + 'workers/uf', { headers: headers });
  }

  getByStatus(status) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(environment.apiUrl + 'workers/status/' + status, { headers: headers });
  }

  getByRegistration(registration) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(environment.apiUrl + 'workers/registration/' + registration, { headers: headers });
  }

  getBySalary(lowest, highest) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(environment.apiUrl + 'workers/salary/' + lowest + '/' + highest, { headers: headers });
  }

  getBySalaryLowest(lowest) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(environment.apiUrl + 'workers/salary/' + lowest, { headers: headers });
  }

  addWorker(worker) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.post(environment.apiUrl + 'workers', worker, { headers: headers });
  }

  deleteWorker(cpf) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.delete(environment.apiUrl + 'workers/' + cpf, { headers: headers });
  }


}
