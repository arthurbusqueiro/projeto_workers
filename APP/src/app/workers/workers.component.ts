import { Component } from '@angular/core';
import { WorkersService } from './workers.service';
import { debug } from 'util';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.css']
})
export class WorkersComponent {
  displayConfirmDeletion: boolean;
  workerForm: FormGroup;
  totalUF: number;
  workersUF: any;
  loadingUF: boolean;
  display: boolean;
  displayNew: boolean;
  loading: boolean;
  workers: any[];
  workerEdit: any;
  cols = [
    {
      field: 'Name',
      header: 'Nome'
    },
    {
      field: 'CPF',
      header: 'CPF'
    },
    {
      field: 'Job',
      header: 'Cargo'
    },
    {
      field: 'UF',
      header: 'UF de Nascimento'
    },
    {
      field: 'RegistrationDate',
      header: 'Data de Cadastro'
    },
    {
      field: 'Status',
      header: 'Status'
    },
    {
      field: 'Salary',
      header: 'Salário'
    },
  ];
  yearFilter: number;
  rangeValues = [0, 20000];
  name = null;
  cpf = null;
  job = null;
  status = null;
  registration = null;
  colsUF = [
    {
      field: '_id',
      header: 'UF de Nascimento'
    },
    {
      field: 'flatsCount',
      header: 'Quantidade de Funcionários'
    },
  ];
  filter: string;

  constructor(private _workersService: WorkersService, private _router: Router, private messageService: MessageService) {
    this.workerForm = new FormGroup({
      Name: new FormControl(
        { value: '', disabled: false }, Validators.compose([Validators.required])
      ),
      CPF: new FormControl(
        { value: '', disabled: false }, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])
      ),
      Job: new FormControl(
        { value: '', disabled: false }, Validators.compose([Validators.required])
      ),
      Status: new FormControl(
        { value: 'ATIVO', disabled: false }, Validators.compose([Validators.required])
      ),
      Salary: new FormControl(
        { value: 0, disabled: false }, Validators.compose([Validators.required])
      ),
      RegistrationDate: new FormControl(
        { value: new Date(), disabled: false }, Validators.compose([Validators.required])
      ),
      UF: new FormControl(
        { value: 'SP', disabled: false }, Validators.compose([Validators.required])
      ),
      _id: new FormControl(
        { value: null, disabled: false }
      ),
    });
    this.workers = [];
    this.findWorkers();
    this.groupByUF();
  }

  findWorkers() {
    this.filter = undefined;
    this.loading = true;
    this._workersService.getAll().subscribe(
      (response: any) => {
        this.loading = false;
        this.workers = response.workers;
      },
      err => {
        this.loading = false;
        console.log(err);
      }
    );
  }

  getByName(name) {
    if (name) {
      this.filter = 'Name';
      this.loading = true;
      this.cpf = null;
      this.job = null;
      this.registration = null;
      this.status = null;
      this.rangeValues = [0, 20000];
      this._workersService.findByName(name).subscribe(
        (response: any) => {
          this.loading = false;
          this.workers = response.workers;
        },
        err => {
          this.loading = false;
          console.log(err);
        }
      );
    } else {
      this.findWorkers();
    }
  }

  getByCPF(cpf) {
    if (cpf) {
      this.filter = 'CPF';
      this.loading = true;
      this.name = null;
      this.job = null;
      this.registration = null;
      this.status = null;
      this.rangeValues = [0, 20000];
      this._workersService.findByCPF(cpf).subscribe(
        (response: any) => {
          this.loading = false;
          this.workers = response.workers;
        },
        err => {
          this.loading = false;
          console.log(err);
        }
      );
    } else {
      this.findWorkers();
    }
  }

  getByJob(job) {
    if (job) {
      this.filter = 'Job';
      this.cpf = null;
      this.loading = true;
      this.name = null;
      this.registration = null;
      this.status = null;
      this.rangeValues = [0, 20000];
      this._workersService.findByJob(job).subscribe(
        (response: any) => {
          this.loading = false;
          this.workers = response.workers;
        },
        err => {
          this.loading = false;
          console.log(err);
        }
      );
    } else {
      this.findWorkers();
    }
  }

  getByRegistration(registration) {
    if (registration) {
      this.filter = 'RegistrationDate';
      this.loading = true;
      this.cpf = null;
      this.job = null;
      this.name = null;
      this.status = null;
      this.rangeValues = [0, 20000];
      this._workersService.getByRegistration(registration).subscribe(
        (response: any) => {
          this.loading = false;
          this.workers = response.workers;
        },
        err => {
          this.loading = false;
          console.log(err);
        }
      );
    } else {
      this.findWorkers();
    }
  }

  getByStatus(status) {
    if (status) {
      this.filter = 'Status';
      this.loading = true;
      this.name = null;
      this.job = null;
      this.registration = null;
      this.name = null;
      this.rangeValues = [0, 20000];
      this._workersService.getByStatus(status).subscribe(
        (response: any) => {
          this.loading = false;
          this.workers = response.workers;
        },
        err => {
          this.loading = false;
          console.log(err);
        }
      );
    } else {
      this.findWorkers();
    }
  }

  getBySalary() {
    if (this.rangeValues) {
      this.filter = 'Salary';
      this.loading = true;
      this.name = null;
      this.job = null;
      this.registration = null;
      this.status = null;
      this.name = null;
      if (this.rangeValues[0] === this.rangeValues[1]) {
        this.getBySalaryLowest(this.rangeValues[0]);
      } else {
        this._workersService.getBySalary(this.rangeValues[0], this.rangeValues[1]).subscribe(
          (response: any) => {
            this.loading = false;
            this.workers = response.workers;
          },
          err => {
            this.loading = false;
            console.log(err);
          }
        );
      }
    } else {
      this.findWorkers();
    }
  }

  getBySalaryLowest(value) {
    if (value) {
      this.loading = true;
      this._workersService.getBySalaryLowest(value).subscribe(
        (response: any) => {
          this.loading = false;
          this.workers = response.workers;
        },
        err => {
          this.loading = false;
          console.log(err);
        }
      );
    } else {
      this.findWorkers();
    }
  }

  showDialog() {
    this.display = true;
  }

  groupByUF() {
    this._workersService.groupByUF().subscribe(
      (response: any) => {
        this.workersUF = response.workers;
        this.totalUF = 0.0;
        this.workersUF.forEach(element => {
          this.totalUF += element.flatsCount
        });
      },
      err => {
        this.loading = false;
        console.log(err);
      }
    );
  }

  addNew() {
    this.displayNew = true;
  }

  createWorker() {
    this._workersService.addWorker(this.workerForm.value).subscribe(
      (response: any) => {
        this.displayNew = false;
        Object.keys(this.workerForm.value).forEach(key => {
          this.workerForm.get(key).setValue('');
        });
        this.showSuccess('Funcionário ' + this.workerForm.get('Name').value + ' incluído/editado com sucesso.');
        this.resetEdit();
        this.keepFilters();
      },
      err => {
        this.displayNew = false;
        err.error.message.forEach(element => {
          this.showError(element.message);
        });
      }
    );
  }

  editWorker(worker) {
    this.workerForm.setValue({ Name: worker.Name, CPF: worker.CPF, Job: worker.Job, RegistrationDate: new Date(worker.RegistrationDate), Status: worker.Status, UF: worker.UF, Salary: Number.parseFloat(worker.Salary), _id: worker._id });
    this.displayNew = true;
  }

  showDeletion(worker) {
    this.displayConfirmDeletion = true;
    this.workerEdit = worker;
  }

  deleteWorker() {
    this._workersService.deleteWorker(this.workerEdit.CPF).subscribe(
      (response: any) => {
        this.showError('Funcionário ' + this.workerEdit.Name + ' excluído com sucesso.');
        this.displayConfirmDeletion = false;
        this.keepFilters();
      },
      err => {
        this.displayConfirmDeletion = false;
        console.log(err);
      }
    );
  }

  totalSalaries() {
    let totalSalaries = 0.0;
    this.workers.forEach(worker => {
      totalSalaries += worker.Salary;
    });
    return totalSalaries;
  }

  resetEdit() {
    this.workerForm.setValue({
      Name: '',
      CPF: '',
      Job: '',
      RegistrationDate: new Date(),
      Status: 'ATIVO',
      UF: 'SP',
      Salary: 0,
      _id: null
    });
    this.workerEdit = undefined;
  }

  keepFilters() {
    debugger;
    switch (this.filter) {
      case 'Name':
        this.getByName(this.name);
        break;
      case 'CPF':
        this.getByCPF(this.cpf);
        break;
      case 'Job':
        this.getByJob(this.job);
        break;
      case 'Status':
        this.getByStatus(this.status);
        break;
      case 'RegistrationDate':
        this.getByRegistration(this.registration);
        break;
      case 'Salary':
        this.getBySalary();
        break;
      default:
        break;
    }
  }

  showSuccess(message) {
    this.messageService.add({ severity: 'success', summary: 'Sucesso', detail: message });
  }

  showError(message) {
    this.messageService.add({ severity: 'error', summary: 'Excluído', detail: message });
}
}
