import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkersComponent } from 'src/app/workers/workers.component';

const AppRoutes: Routes = [
    {
      path: '',
      redirectTo: '/workers',
      pathMatch: 'full'
    },
    {
      path: 'workers',
      component: WorkersComponent
    },
  ];

@NgModule({
    imports: [RouterModule.forRoot(AppRoutes, {})],
    exports: [RouterModule]
})
export class AppRouting { }